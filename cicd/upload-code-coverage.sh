#!/usr/bin/env bash

export PY_VERSION=$(python -c "import sys; print('{}{}'.format(*sys.version_info))")
export SRC_REPORT_NAME="Py${PY_VERSION}-Linux-src"
export SRC_REPORT_FLAGS="linux,py${PY_VERSION},src"
export SRC_REPORT_PATH="artifacts/coverage-project.xml"
export TESTS_REPORT_NAME="Py${PY_VERSION}-Linux-tests"
export TESTS_REPORT_FLAGS="linux,py${PY_VERSION},tests"
export TESTS_REPORT_PATH="artifacts/coverage-tests.xml"

if [ "$(which curl)x" == "x" ]; then
    echo "Failed to find the 'curl' binary"
    exit 0
fi

if [ "$(which gpg)x" == "x" ]; then
    echo "Failed to find the 'gpg' binary"
    exit 0
fi

if [ "$(which shasum)x" == "x" ]; then
    echo "Failed to find the 'shasum' binary"
    exit 0
fi

if [ -f "${SRC_REPORT_PATH}" ]; then
    n=0
    until [ "$n" -ge 5 ]
    do
    if curl --max-time 30 -L https://uploader.codecov.io/latest/codecov-linux --output codecov-linux; then
        break
    fi
    n=$((n+1))
    sleep 15
    done
    n=0
    until [ "$n" -ge 5 ]
    do
    if curl --max-time 30 -L https://uploader.codecov.io/latest/codecov-linux.SHA256SUM --output codecov-linux.SHA256SUM; then
        break
    fi
    n=$((n+1))
    sleep 15
    done
    n=0
    until [ "$n" -ge 5 ]
    do
    if curl --max-time 30 -L https://uploader.codecov.io/latest/codecov-linux.SHA256SUM.sig --output codecov-linux.SHA256SUM.sig; then
        break
    fi
    n=$((n+1))
    sleep 15
    done
    n=0
    until [ "$n" -ge 5 ]
    do
    if curl --max-time 30 -L https://keybase.io/codecovsecurity/pgp_keys.asc | gpg --import; then
        break
    fi
    n=$((n+1))
    sleep 15
    done
    gpg --verify codecov-linux.SHA256SUM.sig codecov-linux.SHA256SUM && \
        shasum -a 256 -c codecov-linux.SHA256SUM && \
        chmod +x codecov-linux || exit 0
fi

if [ -f codecov-linux ] && [ -f $SRC_REPORT_PATH ]; then
    ./codecov-linux -R $(pwd) -n "${SRC_REPORT_NAME}" -f "${SRC_REPORT_PATH}" -F "${SRC_REPORT_FLAGS}" || true
fi

if [ -f codecov-linux ] && [ -f $TESTS_REPORT_PATH ]; then
    ./codecov-linux -R $(pwd) -n "${TESTS_REPORT_NAME}" -f "${TESTS_REPORT_PATH}" -F "${TESTS_REPORT_FLAGS}" || true
fi
